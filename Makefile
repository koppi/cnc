# install prefix either /usr or /usr/local on most unix systems
PREFIX ?= /usr

# comment out to disable trajectory calculation functionality
TRAJGEN_CFLAGS := -DHAVE_TRAJGEN
TRAJGEN_LDLIBS :=
TRAJGEN_OBJS   := trajgen.o

NANOPB_CFLAGS  := -DHAVE_NANOPB -I.
NANOPB_LDLIBS  :=
NANOPB_OBJS    := cnc.pb.o pb_encode.o pb_decode.o pb_common.o
NANOPB_PROTOC  := protoc

GRBL_CFLAGS :=
GRBL_LDLIBS :=
GRBL_OBJS   := gcode.o jog.o motion_control.o protocol.o system.o grbl_hal.o nuts_bolts.o planner.o settings.o

ETHERCAT_CFLAGS := -I/opt/machinekit/include -D_FORTIFY_SOURCE=0 -D_GNU_SOURCE -DULAPI
ETHERCAT_LDLIBS := -L/opt/machinekit/lib -llinuxcnchal -llinuxcnc
ETHERCAT_OBJS   := ec.o

CFLAGS  += $(TRAJGEN_CFLAGS) $(NANOPB_CFLAGS) $(GRBL_CFLAGS) $(ETHERCAT_CFLAGS) -O2 -g -Wall -std=gnu11
LDLIBS  += $(TRAJGEN_LDLIBS) $(NANOPB_LDLIBS) $(GRBL_LDLIBS) $(ETHERCAT_LDLIBS) -lm -lrt -lpthread

LDFLAGS += -Wl,--as-needed

all: cnc
#all: cnc.pb.c cnc

cnc.pb.c: cnc.proto
	$(NANOPB_PROTOC) $(NANOPB_PROTOC_OPTS) --nanopb_out=. cnc.proto 1>&2 2>/dev/null

cnc.pb.o: cnc.pb.c
	$(CC) -o $@ -c $(CFLAGS) $+

%.o: %.c
	$(CC) -o $@ -c $(CFLAGS) $+

cnc: cnc.o $(TRAJGEN_OBJS) $(NANOPB_OBJS) $(GRBL_OBJS) $(ETHERCAT_OBJS)
	$(CC) -o $@ $+ $(LDFLAGS) $(LDLIBS)

clean:
	rm -f *.o cnc

pull:
	git pull origin main --rebase

push:
	git push origin HEAD:main
