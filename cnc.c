#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <mqueue.h>
#include <sched.h>
#include <pthread.h>
#include <signal.h>

#include <pb_encode.h>
#include <pb_decode.h>

#include "cnc.pb.h"

#include "grbl.h"
#include "trajgen.h"
#include "ec.h"

real_t tg_speed;
real_t tg_accel;
real_t tg_blending;
unsigned tg_n_joints;
trajgen_t tg_tg;
trajgen_config_t tg_cfg;
pose_t tg_last_pose;
char tg_err_str[1024];

#define QUEUE_NAME  "/test_queue"

static bool th_consumer_running = true;
static bool th_publisher_running = true;

void tg_echk (unsigned merrno) {
  if (merrno) {
    fprintf(stderr, "tg error: %s\n", trajgen_errstr(tg_tg.last_error.tg_errno, tg_err_str, 1024));
    exit(1);
  }
}

void tg_init (real_t max_speed, real_t max_accel, real_t sample_frequency,
    unsigned interpolation_rate, unsigned max_joints)
  {
    memset(tg_err_str, 0, sizeof(tg_err_str));
    memset(&tg_last_pose, 0, sizeof(pose_t));
    memset(&tg_tg, 0, sizeof(trajgen_t));
    memset(&tg_cfg, 0, sizeof(trajgen_config_t));

    tg_n_joints = max_joints > TG_MAX_JOINTS ? TG_MAX_JOINTS : max_joints;
    memset(&tg_cfg, 0, sizeof(trajgen_config_t));
    tg_cfg.number_of_used_joints = tg_n_joints;
    tg_cfg.max_coord_velocity = max_speed;
    tg_cfg.max_coord_acceleration = max_accel;
    tg_cfg.sample_interval = 1.0/sample_frequency;
    tg_cfg.interpolation_rate = interpolation_rate;
    pose_set_all(&tg_cfg.max_manual_velocities, max_speed);
    pose_set_all(&tg_cfg.max_manual_accelerations, max_accel);
    for (unsigned i = 0; i < tg_n_joints; i++) {
      tg_cfg.joints[i].max_velocity = max_speed;
      tg_cfg.joints[i].max_acceleration = max_speed;
    }
    tg_echk(trajgen_initialize(&tg_tg, tg_cfg));
  }

void tg_line(double x, double y, double z, double accel) {
    pose_t p;

    p.x = x; p.y = y; p.z = -z; p.a = 0; p.b = 0; p.c = 0; p.u = 0; p.v = 0; p.w = 0;

    if (p.x != tg_last_pose.x || p.y != tg_last_pose.y || p.z != tg_last_pose.z) {
        //fprintf(stderr, "# line {x:%.6g, y:%.6g, z:%.6g, a:%.6g, b:%.6g, c:%.6g, v:%.6g, u:%.6g, w:%.6g, s:%.6g,  a:%.6g, tol:%.6g }\n", p.x, p.y, p.z, p.a, p.b, p.c, p.u, p.v, p.w, tg_speed, tg_accel, tg_blending);
        tg_echk(trajgen_add_line(p, tg_speed, tg_accel));
        tg_last_pose.x = p.x;
        tg_last_pose.y = p.y;
        tg_last_pose.z = p.z;
    }
}

void consume_output() {
  // Gets the current block. Returns NULL if buffer empty
  plan_block_t *pb = plan_get_current_block();

  while (pb && !trajgen_queue_full()) {
      double dv = plan_get_exec_block_exit_speed_sqr();
      // printf("Move X%d Y%d Z%d speed %2.f\n", pb->steps[0], pb->steps[1], pb->steps[2], sqrt(dv));
#define D 25.0
      tg_line(pb->steps[0] / D, pb->steps[1] / D, pb->steps[2] / D, sqrt(dv));
      plan_discard_current_block();
      pb = plan_get_current_block();
  }

  do {
      if (trajgen_tick() != TRAJ_ERROR_OK) {
          //fprintf(stderr, "Error: %s\n", tg_tg.last_error);
          trajgen_abort();
          exit(1);
      } else {
          //printf("XYZ: %8.2f %8.2f %8.2f\n", tg_tg.joints[0].position, tg_tg.joints[1].position, tg_tg.joints[2].position);
          ec_consume_output(tg_tg);
          //better_sleep(0.0001);
      }
  } while(!tg_tg.is_done);
}

char *remove_white_spaces(char *str)
{
    int i = 0, j = 0;
    while (str[i])
    {
        if (str[i] != ' ')
            str[j++] = str[i];
        i++;
    }
    str[j] = '\0';
    return str;
}

void DumpHex(const void* data, size_t size) {
	char ascii[17];
	size_t i, j;
	ascii[16] = '\0';
	for (i = 0; i < size; ++i) {
		printf("%02X ", ((unsigned char*)data)[i]);
		if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
			ascii[i % 16] = ((unsigned char*)data)[i];
		} else {
			ascii[i % 16] = '.';
		}
		if ((i+1) % 8 == 0 || i+1 == size) {
			printf(" ");
			if ((i+1) % 16 == 0) {
				printf("|  %s \n", ascii);
			} else if (i+1 == size) {
				ascii[(i+1) % 16] = '\0';
				if ((i+1) % 16 <= 8) {
					printf(" ");
				}
				for (j = (i+1) % 16; j < 16; ++j) {
					printf("   ");
				}
				printf("|  %s \n", ascii);
			}
		}
	}
}

SimpleMessage *message_new(char* text) {
    SimpleMessage *message = (SimpleMessage *)malloc(sizeof(SimpleMessage));
    memset(message, 0, sizeof(SimpleMessage));
    if (text != NULL) {
        sprintf(message->text, "%s", text);
    }
    return message;
}

void message_free(SimpleMessage *msg) {
    free(msg);
    msg = NULL;
}

typedef struct _archive archive;
struct _archive{
    size_t size;
    char *data;
};

archive *archive_new(size_t size, const char *data) {
    archive *ar = (archive *)malloc(sizeof(archive));
    ar->data = NULL;
    ar->size = size;
    if (size > 0) {
        ar->data = (char *)malloc(size);
        memcpy(ar->data, data, size);
    }
    return ar;
}

size_t archive_size(archive *ar) { return ar->size; }

char *archive_data(archive *ar) { return ar->data; }

void archive_free(archive *ar) {
    free(ar->data);
    free(ar);
    ar = NULL;
}

archive *message_serialize(const SimpleMessage *msg) {
    uint8_t buffer[1024];

    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

    bool status = pb_encode_ex(&stream, SimpleMessage_fields, msg, PB_ENCODE_DELIMITED);

    if (!status) {
        fprintf(stderr, "Encoding failed: %s\n", PB_GET_ERROR(&stream));
        return NULL;
    }

    size_t len = stream.bytes_written;
    archive *ar = archive_new(len, (char *)buffer);

    //DumpHex(ar->data, ar->size);

    return ar;
}

bool message_unserialize(SimpleMessage *msg, const uint8_t *buffer, size_t message_length) {
    archive *ar = archive_new(message_length, (char *)buffer);

    //DumpHex(buffer, message_length);
    //DumpHex(archive_data(ar), archive_size(ar));
    
    pb_istream_t stream = pb_istream_from_buffer((const pb_byte_t *)archive_data(ar), archive_size(ar));
    bool status = pb_decode_ex(&stream, SimpleMessage_fields, msg, PB_ENCODE_DELIMITED);

    archive_free(ar);    

    if (!status) {
        fprintf(stderr, "Decoding failed: %s\n", PB_GET_ERROR(&stream));
        return false;
    }

    return true;
}

mqd_t get_message_queue (bool read) {
    struct mq_attr attr = {0};

    attr.mq_maxmsg  = 10;
    attr.mq_msgsize = 1024;
    attr.mq_flags   = 0;

    int flgs = read ? O_RDONLY : O_WRONLY;
    mqd_t mqfd = mq_open (QUEUE_NAME, flgs | O_CREAT, 0644, &attr);
    if (mqfd == (mqd_t) - 1) {
        fprintf(stderr, "get_message_queue: ERROR mq_open failed: %d %s\n", errno, strerror(errno));
        exit(1);
    } else {
        //fprintf(stderr, "get message_queue: ok %d\n", mqfd);
    }

    return mqfd;
}

bool send_message_queue(mqd_t mqfd, char *pline) {
    //printf("Sending '%s'\n", pline);

    SimpleMessage *msg = message_new(pline);
    archive *ar = message_serialize(msg);
    //DumpHex(archive_data(ar), archive_size(ar));

    int status = mq_send(mqfd, (const char *)archive_data(ar), archive_size(ar), 42);

    archive_free(ar);    
    
    if (status < 0) {
        printf ("Error: send_message_queue: mq_send=%d %d(%s)\n", status, errno, strerror(errno));
        return false;
    }

    return true;
}

static ssize_t receive_message_queue(mqd_t mqfd, SimpleMessage *msg) {
    struct timespec ts;
    int status = clock_gettime (CLOCK_REALTIME, &ts);
    if (status != 0) {
        printf ("receive_message_queue: ERROR clock_gettime failed\n");
    }
    //  1000000000  // 1 second in ns
    ts.tv_nsec += 200000000;  // 200ms timeout
    if (ts.tv_nsec >= 1000000000) {
        ts.tv_nsec -= 1000000000;
        ts.tv_sec += 1;
    }

    struct mq_attr mqstat;
    mq_getattr(mqfd, &mqstat);
    size_t size = mqstat.mq_msgsize;
    (void)size;
    //printf("msg size = %ld\n", size);

    unsigned char buffer[1024];
    
    int nbytes = mq_timedreceive (mqfd, (char *) &buffer, size, 0, &ts);
    if (nbytes < 0) {
        // mq_receive failed.  If the error is because of EINTR or ETIMEDOUT then it is not a failure. but we return anyway
        if (errno == ETIMEDOUT) {
            // timeout is ok
            return -1;

        } else if (errno != EINTR) {
            printf ("receive_message_queue: ERROR mq_receive failure, errno=%d: %s\n",
                    errno, strerror(errno));
            return -1;

        } else {
            printf ("receive_message_queue: mq_receive interrupted!\n");
            return -1;
        }
    } else {
        //printf("Read %d bytes.\n", nbytes);
        message_unserialize(msg, buffer, nbytes);
        //DumpHex(buffer, nbytes);
    }

    return nbytes;
}

volatile bool uart_comms_eof = false;

static void * uart_comms(void *unused) {
    (void)unused;

    printf("UART Comms thread running\n");

    mqd_t mqfd = get_message_queue(false);

    char line[1024];
    size_t cnt = 0;
    size_t n;
    bool discard = false;
    while(th_publisher_running) {
        n = read(0, &line[cnt], 1);

        if(n == 1) {
            if(line[cnt] == 24) { // ^X
                discard = false;
                cnt = 0;

            } else if(discard) {
                // we discard long lines until we get the newline
                if(line[cnt] == '\n') discard = false;

            } else if(line[cnt] == '\n') {
                line[cnt] = '\0';
                char *l = strdup(line);
                send_message_queue(mqfd, l);
                cnt = 0;
            } else if(line[cnt] == '\r') {
                // ignore CR
                continue;
            } else if(line[cnt] == 8 || line[cnt] == 127) { // BS or DEL
                if(cnt > 0) --cnt;
            } else {
                ++cnt;
            }

        } else {
            if (errno == 0) {
                printf("UART EOF reached.\n");
                uart_comms_eof = true;
            } else {
                printf("UART: read error: %d\n", errno);
            }
            break;
        }
    }

    /* Cleanup */
    uart_comms_eof = true;
    //printf("[PUBLISHER]: Cleanup...\n");
    //mq_close(mqfd);

    printf("UART Comms thread exiting\n");

    return 0;
}

static void *
commandthrd (void *unused) {
    (void)unused;

    printf ("Command thread running\n");

    mqd_t mqfd = get_message_queue(true);

    while(th_consumer_running) {
        SimpleMessage *msg = message_new(NULL);
        bool idle = false;
        (void)idle;

        // This will timeout after 200 ms
        ssize_t bytes = receive_message_queue(mqfd, msg);
        if (bytes >= 0) {
            printf("> %s\n", msg->text);
            gc_execute_line(remove_white_spaces(msg->text));
            //DumpHex(msg->text, bytes);
        } else {
            // timed out or other error
            idle = true;

            if (uart_comms_eof) {
                th_consumer_running = false;
            }
        }
        message_free(msg);
    }

    /* Cleanup */
    printf("[CONSUMER]: Cleanup...\n");
    mq_close(mqfd);
    mq_unlink(QUEUE_NAME);
    
    return NULL;
}

void signal_handler(int signum) {
    fprintf(stderr, "\n\nReceived signal: %d.\nStopping threads...\n", signum);
    th_consumer_running = false;
    th_publisher_running = false;
}

int main(int ac, char *av[]) {

    signal(SIGINT, signal_handler);

    grbl_init();

    ec_init();

    tg_speed = 1500;
    tg_accel = 50000;

    tg_init(tg_speed, tg_accel, 1000.0, 5, 3);
    tg_echk(trajgen_switch_state(TRAJ_STATE_COORDINATED));
    while (!tg_tg.is_done) tg_echk(trajgen_tick());
    tg_blending = 15;
    tg_line(0.0, 0.0, 0.0, 500);
    while (!tg_tg.is_done) tg_echk(trajgen_tick());
    
    struct sched_param sched;
    sched.sched_priority = 8;
    if ( sched_setscheduler(getpid(), SCHED_FIFO, &sched) < 0 ) {
        fprintf(stderr, "Error: SETSCHEDULER failed: %d %s\n", errno, strerror(errno));
    }

    pthread_t uart_thread, command_thread;
    void *result;
    pthread_attr_t attr;
    struct sched_param sparam;
    int status;

    status = pthread_attr_init(&attr);
    if (status != 0) {
        printf("main: pthread_attr_init failed, status=%d\n", status);
    }

    status = pthread_attr_setstacksize(&attr, 10000);
    if (status != 0) {
        printf("main: pthread_attr_setstacksize failed, status=%d\n", status);
    }

    status = pthread_attr_setschedpolicy(&attr, SCHED_RR);
    if (status != 0) {
        printf("main: pthread_attr_setschedpolicy failed, status=%d\n", status);
    } else {
        printf("main: Set command thread policy to SCHED_RR\n");
    }

    sparam.sched_priority = 90; // set lower than comms threads... 150; // (prio_min + prio_mid) / 2;
    status = pthread_attr_setschedparam(&attr, &sparam);
    if (status != 0) {
        printf("main: pthread_attr_setschedparam failed, status=%d\n", status);
    } else {
        printf("main: Set command thread priority to %d\n", sparam.sched_priority);
    }

    home_z();
    home_xy();

    status = pthread_create(&command_thread, &attr, commandthrd, NULL);
    if (status != 0) {
        printf("main: pthread_create failed, status=%d\n", status);
    }

    pthread_create(&uart_thread, NULL, &uart_comms, NULL);
    pthread_join(uart_thread, NULL);
    pthread_join(command_thread, &result);

    printf("Exiting.\n");

    ec_exit();

    exit(0);
}
