#include <hal.h>
#include <hal_priv.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>   /* Standard types */
#include <signal.h>
#include <math.h>
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>    /* File control definitions */
#include <ctype.h>
#include <values.h>             // DBL_MAX, maybe
#include <limits.h>             // DBL_MAX, maybe
#include <stdarg.h>
#include <sys/stat.h>           // struct stat, stat()
#include <unistd.h>

#include <time.h>

#include "ec.h"

extern hal_sig_t *halpr_find_sig_by_name(const char *name);

hal_sig_t *sigx;
hal_sig_t *sigy;
hal_sig_t *sigz;

hal_sig_t *sig_pos_fb_x;
hal_sig_t *sig_pos_fb_y;
hal_sig_t *sig_pos_fb_z;

hal_sig_t *hx;
hal_sig_t *hy;
hal_sig_t *hz;

real_t home_offset[3];

int better_sleep (double sleep_time) {
    struct timespec tv;
    /* Construct the timespec from the number of whole seconds... */
    tv.tv_sec = (time_t) sleep_time;
    /* ... and the remainder in nanoseconds. */
    tv.tv_nsec = (long) ((sleep_time - tv.tv_sec) * 1e+9);

    while (1)
    {
        /* Sleep for the time specified in tv. If interrupted by a
           signal, place the remaining time left to sleep back into tv. */
        int rval = nanosleep (&tv, &tv);
        if (rval == 0)
            /* Completed the entire sleep time; all done. */
            return 0;
        else if (errno == EINTR)
            /* Interrupted by a signal. Try again. */
            continue;
        else
            /* Some other error; bail out. */
            return rval;
    }
    return 0;
}

void home_z() {
    hal_bit_t valhz;

    valhz = get_bit_value(&(hz->value));

    while(valhz != 1) {
        set_float_value(&(sigz->value), get_float_value(&(sigz->value)) + 0.02);
        better_sleep(0.002);
        valhz = get_bit_value(&(hz->value));
    }

    home_offset[2] = get_float_value(&(sigz->value));
}

void home_xy() {
    hal_bit_t valhx;
    hal_bit_t valhy;

    valhx = get_bit_value(&(hx->value));
    valhy = get_bit_value(&(hy->value));

    while(valhx != 1 || valhy != 1) {
        if (valhx != 1)
            set_float_value(&(sigx->value), get_float_value(&(sigx->value)) + 0.02);
        if (valhy != 1)
            set_float_value(&(sigy->value), get_float_value(&(sigy->value)) + 0.02);

        better_sleep(0.002);
        valhx = get_bit_value(&(hx->value));
        valhy = get_bit_value(&(hy->value));

        //printf("home xy: %d %d xy: %8.2f %8.2f \n", valhx, valhy, get_float_value(&(sigx->value)), get_float_value(&(sigy->value)));
    }

    home_offset[0] = get_float_value(&(sigx->value));
    home_offset[1] = get_float_value(&(sigy->value));
}

void ec_consume_output(trajgen_t tg_tg) {
    //printf("XYZ: %8.2f %8.2f %8.2f\n", tg_tg.joints[0].position, tg_tg.joints[1].position, tg_tg.joints[2].position);
    set_float_value(&(sigx->value), home_offset[0] + tg_tg.joints[0].position);
    set_float_value(&(sigy->value), home_offset[1] + tg_tg.joints[1].position);
    set_float_value(&(sigz->value), home_offset[2] + tg_tg.joints[2].position);

    volatile double x_fb = get_float_value(&(sig_pos_fb_x->value));
    volatile double y_fb = get_float_value(&(sig_pos_fb_y->value));
    volatile double z_fb = get_float_value(&(sig_pos_fb_z->value));

    while(fabs(home_offset[0] + tg_tg.joints[0].position - x_fb) > 0.001 &&
          fabs(home_offset[1] + tg_tg.joints[1].position - y_fb) > 0.001 &&
          fabs(home_offset[2] + tg_tg.joints[2].position - z_fb) > 0.001 ) {
        
        /*printf(" dx dy dz: %6.3f %6.3f %6.3f\n", fabs(home_offset[0] + tg_tg.joints[0].position - x_fb),
          fabs(home_offset[1] + tg_tg.joints[1].position - y_fb),
          fabs(home_offset[2] + tg_tg.joints[2].position - z_fb));*/
              
        better_sleep(0.00001);
        x_fb = get_float_value(&(sig_pos_fb_x->value));
        y_fb = get_float_value(&(sig_pos_fb_y->value));
        z_fb = get_float_value(&(sig_pos_fb_z->value));
        (void)x_fb; (void)y_fb; (void)z_fb;
    }
}

int comp_id;

int ec_init() {
    comp_id = hal_init("cnc01");

    sigx = halpr_find_sig_by_name("x-pos-cmd");
    //set_float_value(&(sigx->value), 0.0);
    sigy = halpr_find_sig_by_name("y-pos-cmd");
    //set_float_value(&(sigy->value), 0.0);
    sigz = halpr_find_sig_by_name("z-pos-cmd");
    //set_float_value(&(sigz->value), 0.0);

    sig_pos_fb_x = halpr_find_sig_by_name("x-pos-fb");
    sig_pos_fb_y = halpr_find_sig_by_name("y-pos-fb");
    sig_pos_fb_z = halpr_find_sig_by_name("z-pos-fb");

    hx = halpr_find_sig_by_name("x-home-sw-in");
    hy = halpr_find_sig_by_name("y-home-sw-in");
    hz = halpr_find_sig_by_name("z-home-sw-in");

    home_z();
    home_xy();
    printf("home offset: %8.2f %8.2f %8.2f\n", home_offset[0], home_offset[1], home_offset[2]);

    return 0;
}

int ec_exit() {
    hal_exit(comp_id);
    
    return 0;
}
