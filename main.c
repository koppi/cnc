#include <hal.h>
#include <hal_priv.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>   /* Standard types */
#include <signal.h>
#include <math.h>
#include <unistd.h>   /* UNIX standard function definitions */
#include <fcntl.h>    /* File control definitions */
#include <ctype.h>
#include <values.h>             // DBL_MAX, maybe
#include <limits.h>             // DBL_MAX, maybe
#include <stdarg.h>
#include <sys/stat.h>           // struct stat, stat()
#include <unistd.h>

#include <time.h>

#include "grbl.h"
#include "trajgen.h"
#include "ec.h"

extern hal_sig_t *halpr_find_sig_by_name(const char *name);

hal_sig_t *sigx;
hal_sig_t *sigy;
hal_sig_t *sigz;

hal_sig_t *sig_pos_fb_x;
hal_sig_t *sig_pos_fb_y;
hal_sig_t *sig_pos_fb_z;

hal_sig_t *hx;
hal_sig_t *hy;
hal_sig_t *hz;

real_t home_offset[3];

real_t tg_speed;
real_t tg_accel;
real_t tg_blending;
unsigned tg_n_joints;
trajgen_t tg_tg;
trajgen_config_t tg_cfg;
pose_t tg_last_pose;
char tg_err_str[1024];

void tg_echk (unsigned merrno) {
  if (merrno) {
    fprintf(stderr, "tg error: %s\n", trajgen_errstr(tg_tg.last_error.tg_errno, tg_err_str, 1024));
    exit(1);
  }
}

void tg_init (real_t max_speed, real_t max_accel, real_t sample_frequency,
    unsigned interpolation_rate, unsigned max_joints)
  {
    memset(tg_err_str, 0, sizeof(tg_err_str));
    memset(&tg_last_pose, 0, sizeof(pose_t));
    memset(&tg_tg, 0, sizeof(trajgen_t));
    memset(&tg_cfg, 0, sizeof(trajgen_config_t));

    tg_n_joints = max_joints > TG_MAX_JOINTS ? TG_MAX_JOINTS : max_joints;
    memset(&tg_cfg, 0, sizeof(trajgen_config_t));
    tg_cfg.number_of_used_joints = tg_n_joints;
    tg_cfg.max_coord_velocity = max_speed;
    tg_cfg.max_coord_acceleration = max_accel;
    tg_cfg.sample_interval = 1.0/sample_frequency;
    tg_cfg.interpolation_rate = interpolation_rate;
    pose_set_all(&tg_cfg.max_manual_velocities, max_speed);
    pose_set_all(&tg_cfg.max_manual_accelerations, max_accel);
    for (unsigned i = 0; i < tg_n_joints; i++) {
      tg_cfg.joints[i].max_velocity = max_speed;
      tg_cfg.joints[i].max_acceleration = max_speed;
    }
    tg_echk(trajgen_initialize(&tg_tg, tg_cfg));
  }

void tg_line(double x, double y, double z, double accel) {
    pose_t p;

    p.x = x; p.y = y; p.z = -z; p.a = 0; p.b = 0; p.c = 0; p.u = 0; p.v = 0; p.w = 0;
    
    if (p.x != tg_last_pose.x || p.y != tg_last_pose.y || p.z != tg_last_pose.z) {
        fprintf(stderr, "# line {x:%.6g, y:%.6g, z:%.6g, a:%.6g, b:%.6g, c:%.6g, v:%.6g, u:%.6g, "
                "w:%.6g, s:%.6g,  a:%.6g, tol:%.6g }\n", p.x, p.y, p.z, p.a, p.b, p.c, p.u, p.v, p.w,
                tg_speed, tg_accel, tg_blending);
        //fprintf(stderr, "# line {x:%8.2f, y:%8.2f, z:%8.2f, v:%8.2f, a:%8.2f, tol:%8.2f }\n", p.x, p.y, p.z, tg_speed, accel, tg_blending);
        tg_echk(trajgen_add_line(p, tg_speed, tg_accel));
        tg_last_pose.x = p.x;
        tg_last_pose.y = p.y;
        tg_last_pose.z = p.z;
    }
}

int better_sleep (double sleep_time) {
    struct timespec tv;
    /* Construct the timespec from the number of whole seconds... */
    tv.tv_sec = (time_t) sleep_time;
    /* ... and the remainder in nanoseconds. */
    tv.tv_nsec = (long) ((sleep_time - tv.tv_sec) * 1e+9);

    while (1)
    {
        /* Sleep for the time specified in tv. If interrupted by a
           signal, place the remaining time left to sleep back into tv. */
        int rval = nanosleep (&tv, &tv);
        if (rval == 0)
            /* Completed the entire sleep time; all done. */
            return 0;
        else if (errno == EINTR)
            /* Interrupted by a signal. Try again. */
            continue;
              else
                  /* Some other error; bail out. */
                  return rval;
    }
    return 0;
}

void home_z() {
    hal_bit_t valhz;

    valhz = get_bit_value(&(hz->value));

    while(valhz != 1) {
        set_float_value(&(sigz->value), get_float_value(&(sigz->value)) + 0.02);
        better_sleep(0.002);
        valhz = get_bit_value(&(hz->value));
    }

    home_offset[2] = get_float_value(&(sigz->value));
}

void home_xy() {
    hal_bit_t valhx;
    hal_bit_t valhy;

    valhx = get_bit_value(&(hx->value));
    valhy = get_bit_value(&(hy->value));

    while(valhx != 1 || valhy != 1) {
        if (valhx != 1)
            set_float_value(&(sigx->value), get_float_value(&(sigx->value)) + 0.02);
        if (valhy != 1)
            set_float_value(&(sigy->value), get_float_value(&(sigy->value)) + 0.02);

        better_sleep(0.002);
        valhx = get_bit_value(&(hx->value));
        valhy = get_bit_value(&(hy->value));

        //printf("home xy: %d %d xy: %8.2f %8.2f \n", valhx, valhy, get_float_value(&(sigx->value)), get_float_value(&(sigy->value)));
    }

    home_offset[0] = get_float_value(&(sigx->value));
    home_offset[1] = get_float_value(&(sigy->value));
}


void consume_output() {
  // Gets the current block. Returns NULL if buffer empty
  plan_block_t *pb = plan_get_current_block();

  while (pb && !trajgen_queue_full()) {
      double dv = plan_get_exec_block_exit_speed_sqr();
      // printf("Move X: %d, Y:, %d, Z: %d, speed_squared: %2.f\n", pb->steps[0], pb->steps[1], pb->steps[2], dv);
#define D 25.0
      tg_line(pb->steps[0] / D, pb->steps[1] / D, pb->steps[2] / D, dv);
      plan_discard_current_block();
      pb = plan_get_current_block();
  }

  do {
      if (trajgen_tick() != TRAJ_ERROR_OK) {
          fprintf(stderr, "Error: %s\n", tg_tg.last_error);
          trajgen_abort();
          exit(1);
      } else {
          //printf("XYZ: %8.2f %8.2f %8.2f\n", tg_tg.joints[0].position, tg_tg.joints[1].position, tg_tg.joints[2].position);
          set_float_value(&(sigx->value), home_offset[0] + tg_tg.joints[0].position);
          set_float_value(&(sigy->value), home_offset[1] + tg_tg.joints[1].position);
          set_float_value(&(sigz->value), home_offset[2] + tg_tg.joints[2].position);

          volatile double x_fb = get_float_value(&(sig_pos_fb_x->value));
          volatile double y_fb = get_float_value(&(sig_pos_fb_y->value));
          volatile double z_fb = get_float_value(&(sig_pos_fb_z->value));

          while(fabs(home_offset[0] + tg_tg.joints[0].position - x_fb) > 0.001 &&
                fabs(home_offset[1] + tg_tg.joints[1].position - y_fb) > 0.001 &&
                fabs(home_offset[2] + tg_tg.joints[2].position - z_fb) > 0.001 ) {

              
              /*printf(" dx dy dz: %6.3f %6.3f %6.3f\n", fabs(home_offset[0] + tg_tg.joints[0].position - x_fb),
                     fabs(home_offset[1] + tg_tg.joints[1].position - y_fb),
                     fabs(home_offset[2] + tg_tg.joints[2].position - z_fb));*/
              
              better_sleep(0.001);
              x_fb = get_float_value(&(sig_pos_fb_x->value));
              y_fb = get_float_value(&(sig_pos_fb_y->value));
              z_fb = get_float_value(&(sig_pos_fb_z->value));
              (void)x_fb; (void)y_fb; (void)z_fb;
          }
      }
  } while(!tg_tg.is_done);
}

char *remove_white_spaces(char *str)
{
    int i = 0, j = 0;
    while (str[i])
    {
        if (str[i] != ' ')
            str[j++] = str[i];
        i++;
    }
    str[j] = '\0';
    return str;
}

const int MAX_LINE_SIZE = 256;

int main(int argc, char** argv) {
    (void)argc;
    (void)argv;

    char line[MAX_LINE_SIZE + 2];
    int length;

    grbl_init();

    srand(time(NULL));
    
    int comp_id = hal_init("cnc01");

    sigx = halpr_find_sig_by_name("x-pos-cmd");
    //set_float_value(&(sigx->value), 0.0);
    sigy = halpr_find_sig_by_name("y-pos-cmd");
    //set_float_value(&(sigy->value), 0.0);
    sigz = halpr_find_sig_by_name("z-pos-cmd");
    //set_float_value(&(sigz->value), 0.0);

    sig_pos_fb_x = halpr_find_sig_by_name("x-pos-fb");
    sig_pos_fb_y = halpr_find_sig_by_name("y-pos-fb");
    sig_pos_fb_z = halpr_find_sig_by_name("z-pos-fb");

    hx = halpr_find_sig_by_name("x-home-sw-in");
    hy = halpr_find_sig_by_name("y-home-sw-in");
    hz = halpr_find_sig_by_name("z-home-sw-in");

    home_z();
    home_xy();
    printf("home offset: %8.2f %8.2f %8.2f\n", home_offset[0], home_offset[1], home_offset[2]);

    tg_speed = 1500;
    tg_accel = 50000;

    tg_init(tg_speed, tg_accel, 1000.0, 5, 3);
    tg_echk(trajgen_switch_state(TRAJ_STATE_COORDINATED));
    while (!tg_tg.is_done) tg_echk(trajgen_tick());
    tg_blending = 15;
    tg_line(0.0, 0.0, 0.0, 500);
    while (!tg_tg.is_done) tg_echk(trajgen_tick());

    /*
    gc_execute_line("G0 G49 G40  G17 G80 G50 G90");
    gc_execute_line("M6 T0");
    gc_execute_line("G21 (mm)");
    gc_execute_line("M03 S500");
    gc_execute_line("M08");
    gc_execute_line("G90");*/

    while (fgets(line, sizeof(line), stdin) != NULL) {
        length=strlen(line);
        if (length==MAX_LINE_SIZE - 1 && line[length - 1] != '\n') {
             printf("Error, line overeached buffer!\n");
             return 1;
        }
        
        if (line[length - 1]=='\n') {
             line[length - 1]='\0';
             //printf("%s\n", remove_white_spaces(line));
             gc_execute_line(remove_white_spaces(line));
        }
    }

    gc_execute_line("M30");

    hal_exit(comp_id);
    
    return 0;
}
